# Jaeger

A deployment of [Jaeger](https://github.com/jaegertracing/jaeger).

[Getting started Guide](https://www.jaegertracing.io/docs/latest/getting-started/#all-in-one-docker-image)

## Usage

run `make` for usage
