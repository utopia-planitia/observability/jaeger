apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: jaeger
  namespace: jaeger
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
spec:
  tls:
    - hosts:
        - 'jaeger.{{ .Cluster.Domain }}'
      secretName: '{{ .Cluster.Name }}-tls'
  rules:
    - host: 'jaeger.{{ .Cluster.Domain }}'
      http:
        paths:
          - path: /
            backend:
              serviceName: jaeger
              servicePort: 16686
---
apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: jaeger
type: Opaque
data:
  auth: '{{ .Cluster.BasicAuth | b64encode }}'
